package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ilp.bean.ProductBean;
import com.ilp.dal.CareProductDao;
import com.ilp.utilities.DbCon;

public class ProductReadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out = response.getWriter();
		
		CareProductDao<ProductBean> productDao = new CareProductDao<ProductBean>(DbCon.getConnection());
		ArrayList<ProductBean> list = productDao.findAll();
		request.setAttribute("prList", list);
		
		request.getRequestDispatcher("../jsp/ProductPages/DisplayProduct.jsp").forward(request, response);
		
	}

	

}
