package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ilp.bean.ProductBean;
import com.ilp.dal.CareProductDao;
import com.ilp.utilities.DbCon;

public class ProductWriteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String stringid = request.getParameter("id");
		if (stringid != null) {
			int id = Integer.parseInt(stringid);
			// instantiating dao class
			CareProductDao<ProductBean> prDao = new CareProductDao<ProductBean>(DbCon.getConnection());
			ProductBean product = prDao.findById(id);
			request.setAttribute("product", product);
//	    	out.println("id found "+ stringid);
			request.getRequestDispatcher("../jsp/ProductPages/UpdateProduct.jsp").forward(request, response);

		} else {
			// out.println("id not found "+ stringid);
			response.sendRedirect("../index.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();

		String action = request.getParameter("action");
		// collection all form data;
		String product_name = request.getParameter("name");
		String product_company = request.getParameter("company");
		String product_type = request.getParameter("type");
		String product_subtype = request.getParameter("subtype");
		String product_details = request.getParameter("details");
		String product_price = request.getParameter("price");

		String id = request.getParameter("id");
		// instantiating dao class
		CareProductDao<ProductBean> prDao = new CareProductDao<ProductBean>(DbCon.getConnection());
		if (action.equals("add")) {
			// creating product bean object
			ProductBean pbObj = new ProductBean(product_name, product_company, product_type, product_subtype,
					product_details, product_price);
			// calling the save method
			int result = prDao.save(pbObj);

			if (result > 0) {
				request.getSession().setAttribute("message", "Product Added Successfully");
				// request.setAttribute("message", "Product Updated");
				response.sendRedirect(request.getContextPath() + "/Product/List");
			} else {
				request.getSession().setAttribute("message", "Product Not Added Successfully");
				// request.setAttribute("message", "Product Updated");
				response.sendRedirect(request.getContextPath() + "/Product/List");
			}
		} else if (action.equals("update") && id != null) {
			int prid = Integer.parseInt(id);
			// creating product bean object
			ProductBean pbObj = new ProductBean(prid, product_name, product_company, product_type, product_subtype,
					product_details, product_price);

			// calling the update method
			prDao.update(pbObj);
			request.getSession().setAttribute("message", "Product Updated Successfully");
			// request.setAttribute("message", "Product Updated");
			response.sendRedirect(request.getContextPath() + "/Product/List");

		} else {
			request.getSession().setAttribute("message", "Product Not Updated Successfully");
			// request.setAttribute("message", "Product Updated");
			response.sendRedirect(request.getContextPath() + "/Product/List");
		}
	}

}
