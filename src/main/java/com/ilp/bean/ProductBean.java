package com.ilp.bean;


public class ProductBean {
	private int productId;
	private String productName;
	private String productCompany;
	private String productType;
	private String productSubType;
	private String productDetails;
	private String productPrice;
	
	
	public ProductBean() {
	}


	public ProductBean(int productId, String productName, String productCompany, String productType,
			String productSubType, String productDetails, String productPrice) {
		this.productId = productId;
		this.productName = productName;
		this.productCompany = productCompany;
		this.productType = productType;
		this.productSubType = productSubType;
		this.productDetails = productDetails;
		this.productPrice = productPrice;
	}
	
	
	public ProductBean(String productName, String productCompany, String productType, String productSubType,
			String productDetails, String productPrice) {
		this.productName = productName;
		this.productCompany = productCompany;
		this.productType = productType;
		this.productSubType = productSubType;
		this.productDetails = productDetails;
		this.productPrice = productPrice;
	}


	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductCompany() {
		return productCompany;
	}
	public void setProductCompany(String productCompany) {
		this.productCompany = productCompany;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProductSubType() {
		return productSubType;
	}
	public void setProductSubType(String productSubType) {
		this.productSubType = productSubType;
	}
	public String getProductDetails() {
		return productDetails;
	}
	public void setProductDetails(String productDetails) {
		this.productDetails = productDetails;
	}
	public String getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}


	@Override
	public String toString() {
		return "ProductBean [productId=" + productId + ", productName=" + productName + ", productCompany="
				+ productCompany + ", productType=" + productType + ", productSubType=" + productSubType
				+ ", productDetails=" + productDetails + ", productPrice=" + productPrice + "]";
	}
	

}
