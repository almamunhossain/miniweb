package com.ilp.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ilp.bean.ProductBean;

public class CareProductDao<T> implements DaoInterface<T>{
	
	private Connection con;
	
	private String query;
	private PreparedStatement pt;
	private ResultSet rs;
	
	
	public CareProductDao(Connection con) {
		super();
		this.con = con;
	}


	@Override
	public ArrayList<ProductBean> findAll() {
		ArrayList<ProductBean> list = new ArrayList<ProductBean>();
		
		try {
			query = "select * from products";
			pt = this.con.prepareStatement(query);
			rs = pt.executeQuery();
			while(rs.next()) {
				ProductBean pb = new ProductBean();
				pb.setProductId(rs.getInt("careproduct_id"));
				pb.setProductName(rs.getString("careproduct_name"));
				pb.setProductCompany(rs.getString("careproduct_company"));
				pb.setProductType(rs.getString("careproduct_type"));
				pb.setProductSubType(rs.getString("careproduct_subtype"));
				pb.setProductDetails(rs.getString("careproduct_details"));
				pb.setProductPrice(rs.getString("price"));
				list.add(pb);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return list;
	}


	@Override
	public ProductBean findById(int id) {
		ProductBean pb = null;
		
		try {
			query = "select * from products where careproduct_id=?";
			pt = this.con.prepareStatement(query);
			pt.setInt(1, id);
			rs = pt.executeQuery();
			if(rs.next()) {
				pb = new ProductBean();
				pb.setProductId(rs.getInt("careproduct_id"));
				pb.setProductName(rs.getString("careproduct_name"));
				pb.setProductCompany(rs.getString("careproduct_company"));
				pb.setProductType(rs.getString("careproduct_type"));
				pb.setProductSubType(rs.getString("careproduct_subtype"));
				pb.setProductDetails(rs.getString("careproduct_details"));
				pb.setProductPrice(rs.getString("price"));
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		return pb;
	}


	@Override
	public void update(ProductBean obj){
		try {
			query = "update products set careproduct_name=?, careproduct_company=?, careproduct_type=?, careproduct_subtype=?, careproduct_details=?, price=? where careproduct_id=?";
			pt = this.con.prepareStatement(query);
			pt.setString(1, obj.getProductName());
			pt.setString(2, obj.getProductCompany());
			pt.setString(3, obj.getProductType());
			pt.setString(4, obj.getProductSubType());
			pt.setString(5, obj.getProductDetails());
			pt.setString(6, obj.getProductPrice());
			pt.setInt(7, obj.getProductId());
			pt.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}


	@Override
	public int save(ProductBean obj) {
		int resp = 0;
		try {
			query="insert into products (careproduct_name,careproduct_company,careproduct_type,careproduct_subtype, careproduct_details, price) values(?,?,?,?,?,?)";
			pt = this.con.prepareStatement(query);
			pt.setString(1, obj.getProductName());
			pt.setString(2, obj.getProductCompany());
			pt.setString(3, obj.getProductType());
			pt.setString(4, obj.getProductSubType());
			pt.setString(5, obj.getProductDetails());
			pt.setString(6, obj.getProductPrice());
			resp = pt.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return resp;
	}


	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	
}
