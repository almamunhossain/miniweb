package com.ilp.dal;
import java.util.*;

import com.ilp.bean.ProductBean;

public interface DaoInterface<T> {
	
	ArrayList<ProductBean> findAll();
	ProductBean findById(int id);
	void update(ProductBean obj);
	int save(ProductBean obj);
	void delete(int id);
	
}
