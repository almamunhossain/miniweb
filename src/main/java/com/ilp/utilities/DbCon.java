package com.ilp.utilities;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbCon {
private static Connection con;
    
    public static Connection getConnection(){
    	
        try{
        	if(con==null) {
                Class.forName("com.mysql.cj.jdbc.Driver");
                con=DriverManager.getConnection("jdbc:mysql://localhost:3306/careproducts","root","almamun@123");
                System.out.println("connected");
        	}
        }catch(Exception e){
            e.printStackTrace();
        }
        return con;
    }
    
    public static void closeConnection() {
    	try {
    		if(con!=null && !con.isClosed()) {
    			con.close();
    		}
    		
    		con = null;
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    }
}
