<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<jsp:include page="/partials/menu.jsp"></jsp:include>
	<form action="${pageContext.request.contextPath}/Product/Write"
		method="post">
		
		<input type="hidden" name="action" value="update">
		<p>
			<label>Product Name</label> <input type="text"
				name="name" value="${product.productName}" required><br>
		</p>
		<p>
			<label>Product Company</label> <input type="text"
				class="form-control" name="company"
				value="${product.productCompany}" required>
		</p>
		<p>
			<label>Product Type</label> <select class="form-control" name="type">
				<option ${product.productType == 'Type One' ? 'selected':'' }>Type One</option>
				<option ${product.productType == 'Type Two' ? 'selected':'' }>Type Two</option>
				<option ${product.productType == 'Type Three' ? 'selected':'' }>Type Three</option>
				<option ${product.productType == 'Type Four' ? 'selected':'' }>Type Four</option>
			</select>
		</p>
		<p>
			<label>Select Subtype</label><br>
				<input class="form-check-input" type="radio" name="subtype"
					id="inlineRadio1" value="Subtype One" ${product.productSubType == 'Subtype One' ? 'checked':'' } required> <label
					for="inlineRadio1">Sub Type One</label>
					
				<input class="form-check-input" type="radio" name="subtype"
					id="inlineRadio2" value="Subtype Two" ${product.productSubType == 'Subtype Two' ? 'checked':'' } required> <label
					 for="inlineRadio2">Sub Type Two</label>
		</p>
		<p>
			<label for="productdeatils">Product Details</label>
			<textarea class="form-control" name="details" id="productdeatils"
				rows="3">${product.productDetails}</textarea>
		</p>
		<p>
			<label>Product Price</label> <input type="number" name="price"
				value="${product.productPrice}" required>
		</p>

		<input type="hidden" name="id" value="${product.productId}">

		<button type="submit" class="btn btn-primary">Update Products</button>
	</form>
</body>
</html>