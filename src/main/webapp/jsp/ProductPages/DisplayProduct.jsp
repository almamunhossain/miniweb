<%@page import="com.ilp.bean.ProductBean"%>
<%@page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Product</title>
</head>
<body>
	<jsp:include page="/partials/menu.jsp"></jsp:include>
	<br>
	<p><% if(request.getSession().getAttribute("message")!=null){
		out.println(request.getSession().getAttribute("message"));
		request.getSession().removeAttribute("messsage");
		} %></p>
	<br>
	<table>
		<thead>
			<tr>
				<th scope="col">Name</th>
				<th scope="col">Company</th>
				<th scope="col">Type</th>
				<th scope="col">Subtype</th>
				<th scope="col">Details</th>
				<th scope="col">Price</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>

			<%
			ArrayList<ProductBean> list = (ArrayList) request.getAttribute("prList");
			for (ProductBean product : list) {
			%>
			<tr>
				<td><%= product.getProductName() %></td>
				<td><%= product.getProductCompany() %></td>
				<td><%= product.getProductType() %></td>
				<td><%= product.getProductSubType() %></td>
				<td><%= product.getProductDetails() %></td>
				<td><%= product.getProductPrice() %></td>
				<td><a href="${ pageContext.request.contextPath }/Product/Write?id=<%= product.getProductId() %>">edit</a></td>
			</tr>
			<%
			}
			%>

		</tbody>
</body>
</html>