<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>AddProduct</title>
</head>
<body>

	<jsp:include page="/partials/menu.jsp"></jsp:include>

	<!-- insert form -->
	<form action="${pageContext.request.contextPath}/Product/Write"
		method="post">
		<input type="hidden" name="action" value="add">
		<p>
			<label>Product Name</label> <input type="text"
				name="name" placeholder="Product Name" required><br>
		</p>
		<p>
		<label>Product Company</label> <input type="text"
			name="company" placeholder="Product Company" required> <br>
		</p>
		<p>
		<label>Product Type</label> <select name="type">
			<option>Type One</option>
			<option>Type Two</option>
			<option>Type Three</option>
			<option>Type Four</option>
		</select> <br>
		</p>
		<p>
			<label>Select Subtype</label><br>
				<input class="form-check-input" type="radio" name="subtype"
					id="inlineRadio1" value="Subtype One" required> <label
					for="inlineRadio1">Sub Type One</label> 
					
				<input type="radio" name="subtype"
					id="inlineRadio2" value="Subtype Two" required> <label
				for="inlineRadio2">Sub Type Two</label>
			

		</p>
		<p>
			<label for="productdeatils">Product Details</label>
			<textarea name="details" id="productdeatils"
				rows="3"></textarea><br>
		</p>
		<p>
			<label>Product Price</label> <input type="number"
				name="price" placeholder="Product Price"
				required><br>
		</p>
		<button type="submit">Add Products</button>
	</form>
</body>
</html>