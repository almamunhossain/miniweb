<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome to CareProduct</title>
</head>
<body>
	<script type="text/javascript">
		window.onload = function() {
			window.location.href = "${ pageContext.request.contextPath}/Product/List";
		}
	</script>
</body>
</html>